using System;
using System.Collections.Generic;

public class ClientsManager
{
    private readonly static Dictionary<string, Client> _clients = new Dictionary<string, Client>();

    public static bool ExistingClientConnected(string id, int status)
    {
        if (_clients.ContainsKey(id))
        {
            _clients[id].Status = status;
            return true;
        }
        return false;
    }

    public static void CreateClient(string id, Client client)
    {
        _clients.Add(id, client);
    }

    public static IEnumerable<Client> GetUsers()
    {
        return _clients.Values;
    }
}