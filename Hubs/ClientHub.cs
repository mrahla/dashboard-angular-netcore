using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.SignalR;

public class ClientHub : Hub
{
    // static Dictionary<string, Client> _clients = new Dictionary<string, Client>();


    public override Task OnConnectedAsync()
    {
        string id = Context.ConnectionId;
        if (!ClientsManager.ExistingClientConnected(id, 1))
        {
            var httpContext = Context.GetHttpContext();
            ClientsManager.CreateClient(id, new Client
            {
                Status = 1,
                IP = GetIp(httpContext)?? "127.0.0.1",
                LocalTime = httpContext.Request.Query["ltime"],
                OS = httpContext.Request.Query["os"],
                TimeZone = httpContext.Request.Query["tz"],
                Browser = httpContext.Request.Query["br"],
                Resolution = httpContext.Request.Query["res"]
            });
        }

        Clients.All.SendAsync("transferclientstdata", ClientsManager.GetUsers());

        return base.OnConnectedAsync();
    }

    public override Task OnDisconnectedAsync(Exception exception)
    {
        ClientsManager.ExistingClientConnected(Context.ConnectionId, 0);
        Clients.All.SendAsync("transferclientstdata", ClientsManager.GetUsers());

        return base.OnDisconnectedAsync(exception);
    }

    private string GetIp(Microsoft.AspNetCore.Http.HttpContext context)
    {
        return context.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress?.ToString() ?? context?.Request?.Headers?["REMOTE_ADDR"] ?? context?.Connection?.RemoteIpAddress?.ToString();
    }

}