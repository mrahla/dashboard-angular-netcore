public class Client
{
    public string IP { get; set; }
    public string OS { get; set; }
    public string LocalTime { get; set; }
    public string TimeZone { get; set; }
    public string Browser { get; set; }
    public string Resolution { get; set; }
    public int Status { get; set; }
}