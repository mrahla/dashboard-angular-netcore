import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { ClientModel } from '../Interfaces/clientmodel.model'
import * as uip from '../../../node_modules/ua-parser-js';

@Injectable()

export class SignalRService {

  //constructor() { }

  public data: ClientModel[];

  private hubConnection: signalR.HubConnection;

  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(`https://localhost:5001/clients?
        ltime=${this.getLocalTime()}
        &os=${this.getOS()}
        &tz=${new Date().toString().split(" ")[5]}
        &br=${this.getBrowser()}
        &res=${window.screen.width * window.devicePixelRatio + "x" + window.screen.height * window.devicePixelRatio}
        `)
      .build();

    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err))
  }

  public addClientsDataListener = () => {
    console.log("listener");
    this.hubConnection.on('transferclientstdata', (data) => {
      this.data = data;
      console.log(data);
    });
  }

  public getLocalTime = () => {
    let d =  new Date();
    return d.toTimeString();
  };

  public getOS = () => {
    let parser = new uip();
    return parser.getOS().name + " " + parser.getOS().version;
  }

  public getBrowser = () => {
    let parser = new uip();
    return parser.getBrowser().name;
  }

}
