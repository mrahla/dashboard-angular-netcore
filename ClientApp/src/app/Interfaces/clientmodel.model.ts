export interface ClientModel {
    IP: string,
    OS: string,
    LocalTime: string,
    TimeZone: string,
    Browser: string,
    Resolution: string,
    Status: number
}
