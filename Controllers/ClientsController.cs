using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

[Route("api/[controller]")]
[ApiController]
public class ClientsController : ControllerBase
{
    //https://code-maze.com/netcore-signalr-angular/
    private IHubContext<ClientHub> _hub;
    public ClientsController(IHubContext<ClientHub> hub)
    {
        _hub = hub;
    }

    public IActionResult Get()
    {  
        _hub.Clients.All.SendAsync("transferclientstdata", ClientsManager.GetUsers());
        return Ok();
    }
}